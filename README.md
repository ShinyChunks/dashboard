Eine kleine Dokumentation bezüglich der Änderungen und des Fortschritts:

Umfunktionierung zu Node.js Anwendung/Server

repo: https://ShinyChunks@bitbucket.org/ShinyChunks/dashboard.git

•	Erstellung eines neuen Angular-Projekts (nach https://angular.io/tutorial^^) in aktueller Angular Version (AngularJS ist veraltet)

Umstieg: https://angular.io/guide/ajs-quick-reference

•	Umwandlung der vorhandenen Files in entsprechendes Format für Angular…

	o	Probleme….

	o	Lösung?: Neuimplementierung Schritt für Schritt… ?

•	einzelne Teile als verschiedene Angular Komponenten implementiert

•	css global in style.css – bei Bedarf können einzelne Komponenten über eigen css files überarbeitet werden

•	zunächst wurde das Ganze erstmal mit „Mock“-Daten implementiert… 

•	Erhalt der Daten vom Server muss noch getestet werden…

•	Alle HTTP-Anfragen sind in patient.service.ts implementiert -> ist für die Kommunikation und Verwaltung der Daten zuständig

	o	als Kommentare sind immer die alten Versionen der Anfragen hinterlegt

Implementierung der Websockets

https://tutorialedge.net/typescript/angular/angular-websockets-tutorial/
https://tutorialedge.net/typescript/angular/angular-socket-io-tutorial/

grobe Architektur: siehe Aufbau-Skizze
