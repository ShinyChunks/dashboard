document.addEventListener('contextmenu', event => event.preventDefault());

jQuery(document).ready(function($){
    $('.cd-btn').on('click', function(event){
        event.preventDefault();
        $('.cd-panel').addClass('is-visible');
    });

    $('.cd-panel').on('click', function(event){
        if( $(event.target).is('.cd-panel') || $(event.target).is('.cd-panel-close') ) {
            $('.cd-panel').removeClass('is-visible');
            event.preventDefault();
        }
    });

    $('.cd-btn-2').on('click', function(event){
        event.preventDefault();
        $('.cd-panel-2').addClass('is-visible');
    });

    $('.cd-panel-2').on('click', function(event){
        if( $(event.target).is('.cd-panel-2') || $(event.target).is('.cd-panel-close-2') ) {
            $('.cd-panel-2').removeClass('is-visible');
            event.preventDefault();
        }
    });

    $('.cd-btn-3').on('click', function(event){
        event.preventDefault();
        $('.cd-panel-3').addClass('is-visible');
    });

    $('.cd-panel-3').on('click', function(event){
        if( $(event.target).is('.cd-panel-3') || $(event.target).is('.cd-panel-close-3') ) {
            $('.cd-panel-3').removeClass('is-visible');
            event.preventDefault();
        }
    });
});