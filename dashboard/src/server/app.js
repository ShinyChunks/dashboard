let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
//the sockets that are connected to the server
let sockets = new Set();
let port = 5000;

// library for handling http requests - much easier then http library
const request = require('request');


//define what happens when connecting to the socket
io.on('connection', (socket) => {

    // Log whenever a user connects
    sockets.add(socket);
    console.log('Socket ' + socket.id + ' added');

    // Log whenever a client disconnects from our websocket server
    socket.on('disconnect', function(){
      console.log('Deleting socket: ' + socket.id);
      sockets.delete(socket);
      console.log('Remaining sockets: ' + sockets.size);
    });

    // When we receive a 'getpatient' event from our client, print out
    // the patient id and then echo the patient data back to our clients
    // using `io.emit()`
    socket.on('patient-query', (patientID) => {
        console.log("Query received for patient id: " + patientID);
        for(const s of sockets) {
          console.log("Emitting patient data for id" + patientID + " to clients");
          io.emit('patient-data', {data: getPatient(patientID)});
        }
    });

    //TODO add all the other getters...

    socket.on('clientdata', data => {
      console.log(data);
    });
});

// Gets the patient data for the given id
// TODO check whether that works :D
function getPatient(patientID) {
  request('http://10.50.5.63:8090/get_oncoflow_patient?patientid=' + patientID,
        { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    return body.explanation;
  });
}

//TODO add other getters

// Initialize our websocket server on port 8090
http.listen(port, () => {
    console.log('Started on port ' + port);
});


// below are the previously used functions for reference
//
// /* PATIENT CONTROLLER */
// app.controller('patientCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/get_oncoflow_patient?patientid=4779029",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         $scope.patient = response.data[0];
//     });
// });
//
// /* DIAGNOSIS CONTROLLER */
// app.controller('diagnosisCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/patients/4779029/diagnoses",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         console.log(response);
//         $scope.diagnosis = response.data;
//     });
// });
//
// /* CHARLSON SCORE CONTROLLER */
// app.controller('charlsonCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/get_oncoflow_charlson?patientid=4779029",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         $scope.cs = response.data.values;
//         $scope.csfactors = response.data.values[0].values;
//         $scope.age = response.data.values[3].value;
//         $scope.survival = response.data.values[1].value * 100;
//     });
// });
//
// /* ANAMNESIS CONTROLLER */
// app.controller('anamnesisCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/get_oncoflow_anamnesis?patientid=4779029",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         $scope.anamnesis = response.data[0];
//     });
// });
//
// /* LABORATORY CONTROLLER */
// app.controller('labCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/get_oncoflow_laboratory?patientid=4779029",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         $scope.lab = response.data.fields;
//     });
// });
//
// /* PATHOLOGY CONTROLLER */
// app.controller('pathologyCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/get_oncoflow_pathology?patientid=4779029",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         console.log(response);
//         $scope.pathology = response.data[2];
//     });
// });
//
// /* TIMELINE CONTROLLER */
// app.controller('procedureOverviewCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/patients/4779029/procedures",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         $scope.procedures = response.data;
//         var tl = response.data;
//         var procedureSteps = [];
//
//         for (i=0; i < tl.length; i++) {
//             var id = tl[i].id;
//             var content = tl[i].type;
//             var date = tl[i].documentcreationdate.substring(0,10);
//             var result = {"id": id, "content": '<a href="#' + id + '">' + content + '</a>', "start": date};
//             procedureSteps.push(result)
//         }
//
//         var container = document.getElementById('visualization-overview');
//         var items = new vis.DataSet(procedureSteps);
//         var options = {
//             zoomable: false,
//             height: 200
//         };
//         var timeline = new vis.Timeline(container, items, options);
//     });
// });
//
// /* TUMOR BOARD CONTROLLER */
// app.controller('tumorboardCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/get_tumorboardinvitation?patientid=4779029",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         $scope.lab = response.data.result[0];
//     });
// });
//
// /* METRICS CONTROLLER */
// app.controller('metricCtrl', function($scope, $http) {
//     $http({
//         method: "GET",
//         url: "http://10.50.5.63:8090/get_oncoflow_completeness_tumorboard?patientid=4779029",
//         headers: {
//             "accept": "application/json",
//             "content-type": "application/json",
//             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
//         },
//         dataType: "json"
//     }).then(function(response) {
//         $scope.iq = response.data.value * 100;
//     });
// });
