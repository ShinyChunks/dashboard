import { Component, OnInit } from '@angular/core';

import { Diagnose } from '../diagnose';
import { PatientService } from '../patient.service';

@Component({
  selector: 'app-diagnoses',
  templateUrl: './diagnoses.component.html',
  styleUrls: ['./diagnoses.component.css']
})
export class DiagnosesComponent implements OnInit {

  diagnose: Diagnose;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    //get diagnose for current patient
    this.patientService.getDiagnose(-1).subscribe(
      diagnose => this.diagnose = diagnose
    );
  }
}
