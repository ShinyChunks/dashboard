import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapyDetailComponent } from './therapy-detail.component';

describe('TherapyDetailComponent', () => {
  let component: TherapyDetailComponent;
  let fixture: ComponentFixture<TherapyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
