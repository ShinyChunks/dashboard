import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MolecularModalComponent } from './molecular-modal.component';

describe('MolecularModalComponent', () => {
  let component: MolecularModalComponent;
  let fixture: ComponentFixture<MolecularModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MolecularModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MolecularModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
