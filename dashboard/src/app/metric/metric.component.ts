import { Component, OnInit } from '@angular/core';

import { PatientService } from '../patient.service';

@Component({
  selector: 'app-metric',
  templateUrl: './metric.component.html',
  styleUrls: ['./metric.component.css']
})
export class MetricComponent implements OnInit {

  completeness: number;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    //get completeness of current patient
    this.patientService.getCompleteness(-1).subscribe(
      completeness => this.completeness = completeness * 100
    );
  }

}
