import { Component, OnInit } from '@angular/core';

import { Patient } from '../patient';
import { PatientService } from '../patient.service';
import { Charlson } from '../charlson';

@Component({
  selector: 'app-charlson-modal',
  templateUrl: './charlson-modal.component.html',
  styleUrls: ['./charlson-modal.component.css']
})

export class CharlsonModalComponent implements OnInit {

  patient: Patient;
  survival: number;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    //get current patient
    this.patientService.getPatient(-1).subscribe(
      patient => this.patient = patient
    );
    this.survival = this.patient.charlson.survival * 100;
  }

}
