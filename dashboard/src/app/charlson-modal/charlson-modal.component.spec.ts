import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharlsonModalComponent } from './charlson-modal.component';

describe('CharlsonModalComponent', () => {
  let component: CharlsonModalComponent;
  let fixture: ComponentFixture<CharlsonModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharlsonModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharlsonModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
