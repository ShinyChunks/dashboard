/**
 *  Class to save procedures.
 */
export class Procedure {
  id: number;
  content: string;
  date: string; //TODO date data type?
  result: string;
}
