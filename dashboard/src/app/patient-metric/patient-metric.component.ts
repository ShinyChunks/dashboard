import { Component, OnInit, OnDestroy } from '@angular/core';

import { Observable, of } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';

import { Patient } from '../patient';
import { PatientService } from '../patient.service';

@Component({
  selector: 'app-patient-metric',
  templateUrl: './patient-metric.component.html',
  styleUrls: ['./patient-metric.component.css']
})
export class PatientMetricComponent implements OnInit, OnDestroy {

  //array of all patients (currently not loaded...)
  patients: Patient[];

  selectedPatient: Patient;

  sub: Subscription;

  patientID: number;

  constructor(private patientService: PatientService) {
    //TODO remove hardcoded patient id
    this.patientID = 4779029;
  }

  ngOnInit() {
    this.sub = this.patientService.getPatient(this.patientID)
        .subscribe(patient => {
          this.selectedPatient = patient;
        });
    //uncomment if all patients should be fetched -> if so, update patient URL in patient.service
    //this.getPatients();

    //set selected patient in patientService as well
    this.patientService.setPatient(this.selectedPatient);
  }

  onSelect(patient: Patient): void {
    this.selectedPatient = patient;
    this.patientService.setPatient(this.selectedPatient);
  }

  getPatients(): void {
    this.patientService.getPatients().subscribe(
      patients => this.patients = patients
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
