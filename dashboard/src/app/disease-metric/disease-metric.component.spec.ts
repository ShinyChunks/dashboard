import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiseaseMetricComponent } from './disease-metric.component';

describe('DiseaseMetricComponent', () => {
  let component: DiseaseMetricComponent;
  let fixture: ComponentFixture<DiseaseMetricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiseaseMetricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiseaseMetricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
