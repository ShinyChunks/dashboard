import { Component, OnInit, Input } from '@angular/core';

import { Pathology } from '../pathology';
import { Patient } from '../patient';
import { PatientService } from '../patient.service';

@Component({
  selector: 'app-disease-metric',
  templateUrl: './disease-metric.component.html',
  styleUrls: ['./disease-metric.component.css']
})
export class DiseaseMetricComponent implements OnInit {

  pathology: Pathology;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    //get pathology information for current patient
    this.patientService.getPathology(-1).subscribe(
      pathology => this.pathology = pathology
    );
  }

}
