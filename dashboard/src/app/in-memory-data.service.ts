import { Patient } from './patient';
import { Pathology } from './pathology';

import { InMemoryDbService } from 'angular-in-memory-web-api';
//mock patients using http requests
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const patients = [
      { id: 11, firstname: 'Dieter', lastname: 'Drollig', age: 55, patho:
        {t: 'T1', n: 'N0', m: 'M0', metastases: 'keine', p16: '', tumortype: '',
         icdgrading: '', r: '', l: '', v: '', pn: '', lymph: ''},
        diagnose: {primary: ['eins', 'zwei'], secondary: ['eins', 'zwei']},
        karnofskyindexpercent: 1, lab:
        { haemoglobin: 0, creatinin: 0, urea: 0, leucocytes: 0,
          neutrophils: 0, crp: 0, albumin: 0, quick: 0}, anamnesis:
          { actualanamnesis: 'leer', nicotineyearpackages: 0, nicotineabstinence: 0,
            profession: 'Tester', allergies: ['keine'] , medicaments: ['keine'] ,
            weightadmission: 80}
      },
      { id: 12, firstname: 'Peter', lastname: 'Lustig', age: 78, patho:
        {t: '', n: '', m: '', metastases: '', p16: '', tumortype: '',
         icdgrading: '', r: '', l: '', v: '', pn: '', lymph: ''},
        diagnose: {primary: ['eins', 'zwei'], secondary: ['eins', 'zwei']},
        karnofskyindexpercent: 1, lab:
        { haemoglobin: 0, creatinin: 0, urea: 0, leucocytes: 0,
          neutrophils: 0, crp: 0, albumin: 0, quick: 0}, anamnesis:
          { actualanamnesis: 'leer', nicotineyearpackages: 0, nicotineabstinence: 0,
            profession: 'Tester', allergies: ['keine'] , medicaments: ['keine'] ,
            weightadmission: 80}
      },
      { id: 13, firstname: 'Ursula', lastname: 'Ulkig', age: 67, patho:
        {t: '', n: '', m: '', metastases: '', p16: '', tumortype: '',
         icdgrading: '', r: '', l: '', v: '', pn: '', lymph: ''},
         diagnose: {primary: ['eins', 'zwei'], secondary: ['eins', 'zwei']},
         karnofskyindexpercent: 1, lab:
         { haemoglobin: 0, creatinin: 0, urea: 0, leucocytes: 0,
           neutrophils: 0, crp: 0, albumin: 0, quick: 0}, anamnesis:
           { actualanamnesis: 'leer', nicotineyearpackages: 0, nicotineabstinence: 0,
             profession: 'Tester', allergies: ['keine'] , medicaments: ['keine'] ,
             weightadmission: 80}
      },
      { id: 14, firstname: 'Angela', lastname: 'Adrett', age: 46, patho:
        {t: '', n: '', m: '', metastases: '', p16: '', tumortype: '',
         icdgrading: '', r: '', l: '', v: '', pn: '', lymph: ''},
         diagnose: {primary: ['eins', 'zwei'], secondary: ['eins', 'zwei']},
         karnofskyindexpercent: 1, lab:
         { haemoglobin: 0, creatinin: 0, urea: 0, leucocytes: 0,
           neutrophils: 0, crp: 0, albumin: 0, quick: 0}, anamnesis:
           { actualanamnesis: 'leer', nicotineyearpackages: 0, nicotineabstinence: 0,
             profession: 'Tester', allergies: ['keine'] , medicaments: ['keine'] ,
             weightadmission: 80}
      },
      { id: 15, firstname: 'Falk', lastname: 'Formidabel', age: 76, patho:
        {t: '', n: '', m: '', metastases: '', p16: '', tumortype: '',
         icdgrading: '', r: '', l: '', v: '', pn: '', lymph: ''},
         diagnose: {primary: ['eins', 'zwei'], secondary: ['eins', 'zwei']},
         karnofskyindexpercent: 1, lab:
         { haemoglobin: 0, creatinin: 0, urea: 0, leucocytes: 0,
           neutrophils: 0, crp: 0, albumin: 0, quick: 0}, anamnesis:
           { actualanamnesis: 'leer', nicotineyearpackages: 0, nicotineabstinence: 0,
             profession: 'Tester', allergies: ['keine'] , medicaments: ['keine'] ,
             weightadmission: 80}
      }
    ];
    return {patients};
  }
}
