import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent } from './app.component';
import { PatientMetricComponent } from './patient-metric/patient-metric.component';
import { DiseaseMetricComponent } from './disease-metric/disease-metric.component';
import { TherapyMetricComponent } from './therapy-metric/therapy-metric.component';
import { TumorboardMetricComponent } from './tumorboard-metric/tumorboard-metric.component';
import { PatientDetailComponent } from './patient-detail/patient-detail.component';
import { DiseaseDetailComponent } from './disease-detail/disease-detail.component';
import { TherapyDetailComponent } from './therapy-detail/therapy-detail.component';
import { CharlsonModalComponent } from './charlson-modal/charlson-modal.component';
import { MolecularModalComponent } from './molecular-modal/molecular-modal.component';
import { DiagnosesComponent } from './diagnoses/diagnoses.component';
import { AnamnesisComponent } from './anamnesis/anamnesis.component';
import { MetricComponent } from './metric/metric.component';
import { LabComponent } from './lab/lab.component';
import { MessagesComponent } from './messages/messages.component';

@NgModule({
  declarations: [
    AppComponent,
    PatientMetricComponent,
    DiseaseMetricComponent,
    TherapyMetricComponent,
    TumorboardMetricComponent,
    PatientDetailComponent,
    DiseaseDetailComponent,
    TherapyDetailComponent,
    CharlsonModalComponent,
    MolecularModalComponent,
    DiagnosesComponent,
    AnamnesisComponent,
    MetricComponent,
    LabComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
