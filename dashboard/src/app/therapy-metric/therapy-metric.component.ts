import { Component, OnInit } from '@angular/core';

import { Procedure } from '../procedure';
import { PatientService } from '../patient.service';

@Component({
  selector: 'app-therapy-metric',
  templateUrl: './therapy-metric.component.html',
  styleUrls: ['./therapy-metric.component.css']
})
export class TherapyMetricComponent implements OnInit {

  procedures: Procedure[];

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    //get procedures for current patient
    //TODO add remaining information ( -> see getProcedures())
    this.patientService.getProcedures(-1).subscribe(
      procedures => this.procedures = procedures
    );
  }

}
