import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapyMetricComponent } from './therapy-metric.component';

describe('TherapyMetricComponent', () => {
  let component: TherapyMetricComponent;
  let fixture: ComponentFixture<TherapyMetricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapyMetricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapyMetricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
