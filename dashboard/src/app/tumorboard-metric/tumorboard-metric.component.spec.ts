import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TumorboardMetricComponent } from './tumorboard-metric.component';

describe('TumorboardMetricComponent', () => {
  let component: TumorboardMetricComponent;
  let fixture: ComponentFixture<TumorboardMetricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TumorboardMetricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TumorboardMetricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
