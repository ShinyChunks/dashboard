import { Component, OnInit } from '@angular/core';

import { PatientService } from '../patient.service';

@Component({
  selector: 'app-tumorboard-metric',
  templateUrl: './tumorboard-metric.component.html',
  styleUrls: ['./tumorboard-metric.component.css']
})
export class TumorboardMetricComponent implements OnInit {

  tumorboard: string;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    this.patientService.getTumoboard(-1).subscribe(
      tumorboard => this.tumorboard = tumorboard
    );
  }

}
