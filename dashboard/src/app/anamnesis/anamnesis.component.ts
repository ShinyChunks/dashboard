import { Component, OnInit } from '@angular/core';

import { PatientService } from '../patient.service';
import { Anamnesis } from '../anamnesis';

@Component({
  selector: 'app-anamnesis',
  templateUrl: './anamnesis.component.html',
  styleUrls: ['./anamnesis.component.css']
})
export class AnamnesisComponent implements OnInit {

  karnofskyindexpercent: number;
  anamnesis: Anamnesis;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    this.patientService.getAnamesis(-1).subscribe(
      anamnesis => this.anamnesis = anamnesis
    );
  }

}
