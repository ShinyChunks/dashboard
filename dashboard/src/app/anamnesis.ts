export class Anamnesis {
  actualanamnesis: string;
  nicotineyearpackages: number;
  nicotineabstinence: number;
  profession: string;
  allergies: string[];
  medicaments: string[];
  weightadmission: number;
}
