export class Charlson {
  cs: number;
  csfactors: string[];
  survival: number;
}
