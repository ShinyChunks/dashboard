import { Pathology } from './pathology';
import { Diagnose } from './diagnose';
import { Lab } from './lab';
import { Anamnesis } from './anamnesis';
import { Charlson } from './charlson';

export class Patient {
  id: number;
  firstname: string;
  lastname: string;
  age: number;
  patho: Pathology;
  diagnose: Diagnose;
  karnofskyindexpercent: number;
  lab: Lab;
  anamnesis: Anamnesis;
  charlson: Charlson;
}
