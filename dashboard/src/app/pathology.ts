export class Pathology {
  t: string;
  n: string;
  m: string;
  metastases: string;
  p16: string;
  tumortype: string;
  icdgrading: string;
  r: string;
  l: string;
  v: string;
  pn: string;
  lymph: string;
}
