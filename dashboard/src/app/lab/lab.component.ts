import { Component, OnInit } from '@angular/core';

import { Lab } from '../lab';
import { Patient } from '../patient';
import { PatientService } from '../patient.service';

@Component({
  selector: 'app-lab',
  templateUrl: './lab.component.html',
  styleUrls: ['./lab.component.css']
})
export class LabComponent implements OnInit {

  lab: Lab;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    //get lab information of current patient
    this.patientService.getLab(-1).subscribe(
      lab => this.lab = lab
    );
  }

}
