export class Lab {
  //TODO check types! number/string?
  haemoglobin: number;
  creatinin: number;
  urea: number;
  leucocytes: number;
  neutrophils: number;
  crp: number;
  albumin: number;
  quick: number;
}
