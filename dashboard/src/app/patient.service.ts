import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { map, tap, catchError } from 'rxjs/operators';
import * as socketIo from 'socket.io-client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

//"local" imports -> dataTypes and so on
import { MessageService } from './message.service';
import { Patient } from './patient';
import { Anamnesis } from './anamnesis';
import { Charlson } from './charlson';
import { Diagnose } from './diagnose';
import { Pathology } from './pathology';
import { Lab } from './lab';
import { Procedure } from './procedure';
import { Socket } from './shared/interfaces';

declare var io : {
  connect(url: string): Socket;
};

@Injectable({
  providedIn: 'root'
})

export class PatientService {

  socket: Socket;
  observer: Observer<any>;
  selectedPatient: Patient;
  //URL to web api
  private apiUrl = 'http://10.50.5.63:8090/';

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
        'authorization': 'Basic YWRtaW46YWRtaW40Mg=='})
  };

  constructor(
    private http: HttpClient/*,
    private messageService: MessageService*/) { }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    // this.messageService.add('PatientService: ' + message);
    // TODO use new message service functions
  }

  /** Get the patietns via http request */
  getPatients(): Observable<Patient[]> {
    const url = '${this.apiUrl}patients';
    return this.http.get<Patient[]> (url, this.httpOptions)
      .pipe(
        tap(patients => this.log('fetched patients')),
        catchError(this.handleError('getPatients', []))
      );
  }

  /** Set the current patient */
  setPatient(patient: Patient): void {
    this.selectedPatient = patient;
    this.log('Selected patient: ' + this.selectedPatient.firstname + ' ' + this.selectedPatient.lastname);
  }

  /** Get the current patient */
  getSelectedPatient(): Patient {
    return this.selectedPatient;
  }

  /**
   *  Get patient data using the websockets
   */
  getPatient(patientID: number) : Observable<Patient> {
    this.socket = socketIo(this.apiUrl);
    this.socket.emit('patient-query', patientID);
    this.socket.on('patient-data', (res) => {
      this.observer.next(res.data);
    });
    //TODO check how this works... Is result correctly parsed?!?!
    return new Observable(observer => {
      this.observer = observer;
    });;
  }

  
  // this is without websockets...
  // /**
  //   * GET patient by id in the form [url]=id (such as ...patientid=4779029).
  //   * Will 404 if id not found
  //   */
  // getPatient(id: number): Observable<Patient> {
  //   const url = '${this.apiUrl}get_oncoflow_patient?patientid=${id}';
  //   //TODO check whether identifiers match => Patient object <-> database
  //   //TODO call all other getters to save entire patient record??
  //   return this.http.get<Patient>(url, this.httpOptions).pipe(
  //     tap(_ => this.log('fetched patient id=${id}')),
  //     catchError(this.handleError<Patient>('getPatient(id=${id})'))
  //   );
  // }

  /**
   *  Get the anamnesis for a patient with a given id
   *  If you want the anamnesis for the current patient use -1 as id!
   */
  getAnamesis(id: number): Observable<Anamnesis> {
    //if there is no id provided use the current patient's id
    if(id<0) id = this.selectedPatient.id;
    const url = '${this.apiUrl}get_oncoflow_anamnesis?patientid=${id}';
    return this.http.get<Anamnesis>(url, this.httpOptions).pipe(
      tap(_ => this.log('fetched anamnesis for patient id=${id}')),
      catchError(this.handleError<Anamnesis>('getAnamesis(id=${id})'))
    );
  }

  /* Charlson Modal
   *  If you want the diagnoses for the current patient use -1 as id!
   - previously:
  $http({
      method: "GET",
      url: "http://10.50.5.63:8090/get_oncoflow_charlson?patientid=4779029",
      headers: {
          "accept": "application/json",
          "content-type": "application/json",
          "authorization": "Basic YWRtaW46YWRtaW40Mg=="
      },
      dataType: "json"
  }).then(function(response) {
      $scope.cs = response.data.values;
      $scope.csfactors = response.data.values[0].values;
      $scope.age = response.data.values[3].value;
      $scope.survival = response.data.values[1].value * 100;
  });
  */
  //TODO check value assignment -> see response above...
  getCharlson(id: number): Observable<Charlson> {
    if(id<0) id = this.selectedPatient.id;
    const url = '${this.apiUrl}get_oncoflow_charlson?patientid=${id}';
    return this.http.get<Charlson>(url, this.httpOptions).pipe(
      tap(_ => this.log('fetched charlson modal for patient id=${id}')),
      catchError(this.handleError<Charlson>('getCharlson(id=${id})'))
    );
  }

  /* diagnoses
  $http({
      method: "GET",
      url: "http://10.50.5.63:8090/patients/4779029/diagnoses",
      headers: {
          "accept": "application/json",
          "content-type": "application/json",
          "authorization": "Basic YWRtaW46YWRtaW40Mg=="
      },
      dataType: "json"
  }).then(function(response) {
      console.log(response);
      $scope.diagnosis = response.data;
  });
  */
  /**
   *  Get the diagnoses for the patient with the given id.
   *  If you want the diagnoses for the current patient use -1 as id!
   */
  getDiagnose(id: number): Observable<Diagnose> {
    if(id<0) id = this.selectedPatient.id;
    const url = '${this.apiUrl}patients/${id}/diagnoses';
    return this.http.get<Diagnose>(url, this.httpOptions).pipe(
      tap(_ => this.log('fetched diagnoses for patient id=${id}')),
      catchError(this.handleError<Diagnose>('getDiagnose(id=${id})'))
    );
  }

  /* Pathology / disease-metric
  $http({
      method: "GET",
      url: "http://10.50.5.63:8090/get_oncoflow_pathology?patientid=4779029",
      headers: {
          "accept": "application/json",
          "content-type": "application/json",
          "authorization": "Basic YWRtaW46YWRtaW40Mg=="
      },
      dataType: "json"
  }).then(function(response) {
      console.log(response);
      $scope.pathology = response.data[2];
  });
  */
  /**
   *  Gets the pathology information of the patient with the given id.
   *  Use -1 as id if you want the information for the current patient.
   */
   getPathology(id: number): Observable<Pathology> {
     if(id<0) id = this.selectedPatient.id;
     const url = '${this.apiUrl}get_oncoflow_pathology?patientid=${id}';
     return this.http.get<Pathology>(url, this.httpOptions).pipe(
       tap(_ => this.log('fetched pathology information for patient id=${id}')),
       catchError(this.handleError<Pathology>('getPathology(id=${id})'))
     );
   }

   /* lab
   $http({
       method: "GET",
       url: "http://10.50.5.63:8090/get_oncoflow_laboratory?patientid=4779029",
       headers: {
           "accept": "application/json",
           "content-type": "application/json",
           "authorization": "Basic YWRtaW46YWRtaW40Mg=="
       },
       dataType: "json"
   }).then(function(response) {
       $scope.lab = response.data.fields;
   });
   */
   /**
    *  Gets the lab information of the patient with the given id.
    *  Use -1 as id if you want the information for the current patient.
    */
    getLab(id: number): Observable<Lab> {
      if(id<0) id = this.selectedPatient.id;
      const url = '${this.apiUrl}get_oncoflow_laboratory?patientid=${id}';
      return this.http.get<Lab>(url, this.httpOptions).pipe(
        tap(_ => this.log('fetched lab information for patient id=${id}')),
        catchError(this.handleError<Lab>('getLab(id=${id})'))
      );
    }

    /* metric
    $http({
        method: "GET",
        url: "http://10.50.5.63:8090/get_oncoflow_completeness_tumorboard?patientid=4779029",
        headers: {
            "accept": "application/json",
            "content-type": "application/json",
            "authorization": "Basic YWRtaW46YWRtaW40Mg=="
        },
        dataType: "json"
    }).then(function(response) {
        $scope.iq = response.data.value * 100;
    });
    */
    /**
     *  Gets the oncoflow completeness for the patient with the given id.
     *  Use -1 as id if you want the information for the current patient.
     */
     getCompleteness(id: number): Observable<number> {
       if(id<0) id = this.selectedPatient.id;
       const url = '${this.apiUrl}get_oncoflow_completeness_tumorboard?patientid=${id}';
       return this.http.get<number>(url, this.httpOptions).pipe(
         tap(_ => this.log('fetched completeness for patient id=${id}')),
         catchError(this.handleError<number>('getCompleteness(id=${id})'))
       );
     }

     /* therapy-metric
     $http({
         method: "GET",
         url: "http://10.50.5.63:8090/patients/4779029/procedures",
         headers: {
             "accept": "application/json",
             "content-type": "application/json",
             "authorization": "Basic YWRtaW46YWRtaW40Mg=="
         },
         dataType: "json"
     }).then(function(response) {
         $scope.procedures = response.data;
         var tl = response.data;
         var procedureSteps = [];

         for (i=0; i < tl.length; i++) {
             var id = tl[i].id;
             var content = tl[i].type;
             var date = tl[i].documentcreationdate.substring(0,10);
             var result = {"id": id, "content": '<a href="#' + id + '">' + content + '</a>', "start": date};
             procedureSteps.push(result)
         }

         var container = document.getElementById('visualization-overview');
         var items = new vis.DataSet(procedureSteps);
         var options = {
             zoomable: false,
             height: 200
         };
         var timeline = new vis.Timeline(container, items, options);
     });
     */
     /**
      *  Gets the procedures for the patient with the given id.
      *  Use -1 as id if you want the information for the current patient.
      */
      getProcedures(id: number): Observable<Procedure[]> {
        if(id<0) id = this.selectedPatient.id;
        const url = '${this.apiUrl}patients/${id}/procedures';
        //TODO add other values that are needed (see above)
        return this.http.get<Procedure[]>(url, this.httpOptions).pipe(
          tap(_ => this.log('fetched procedures for patient id=${id}')),
          catchError(this.handleError<Procedure[]>('getProcedures(id=${id})'))
        );
      }

    /*TODO get data from server
    $http({
        method: "GET",
        url: "http://10.50.5.63:8090/get_tumorboardinvitation?patientid=4779029",
        headers: {
            "accept": "application/json",
            "content-type": "application/json",
            "authorization": "Basic YWRtaW46YWRtaW40Mg=="
        },
        dataType: "json"
    }).then(function(response) {
        $scope.lab = response.data.result[0];
    });
    */
    /**
     *  Gets the tumorboard information of the patient with the given id.
     *  Use -1 as id if you want the information for the current patient.
     */
     //TODO change return type?
     getTumoboard(id: number): Observable<string> {
       if(id<0) id = this.selectedPatient.id;
       const url = '${this.apiUrl}get_tumorboardinvitation?patientid=${id}';
       return this.http.get<string>(url, this.httpOptions).pipe(
         tap(_ => this.log('fetched tumorboard information for patient id=${id}')),
         catchError(this.handleError<string>('getTumorboard(id=${id})'))
       );
     }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
